package user

import (
	"encoding/json"
	"github.com/asaskevich/govalidator"
	"github.com/julienschmidt/httprouter"
	"io"
	"io/ioutil"
	"net/http"
	"simple/api/errorcode"
	"simple/app/user"
)

func validationAdd(bodyIN io.ReadCloser) (user.ArgAdd, error) {
	post := user.ArgAdd{}
	body, err := ioutil.ReadAll(bodyIN)
	if err != nil {
		return post, err
	}
	if err = json.Unmarshal(body, &post); err != nil {
		return post, err
	}
	if _, err = govalidator.ValidateStruct(post); err != nil {
		return post, err
	}
	return post, nil
}

func (u *User) Add(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	post, err := validationAdd(r.Body)
	if err != nil {
		errorcode.WriteError(errorcode.CodeDataInvalid, err.Error(), w)
		return
	}
	if err = u.op.Add(post); err != nil {
		errorcode.WriteError(errorcode.CodeUnexpected, err.Error(), w)
		return
	}
}
