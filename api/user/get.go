package user

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"simple/api/errorcode"
)

func (u *User) Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	users, err := u.op.List()
	if err != nil {
		errorcode.WriteError(errorcode.CodeUnexpected, err.Error(), w)
		return
	}
	jData, err := json.Marshal(users)
	if err != nil {
		errorcode.WriteError(errorcode.CodeUnexpected, err.Error(), w)
		return
	}
	w.Write(jData)
}
