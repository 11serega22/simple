package middlware

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	error2 "simple/api/errorcode"
)

type Middleware struct{}

func NewMiddleware() Middleware {
	return Middleware{}
}

func (m *Middleware) Auth(next httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		authorizationHeader := r.Header.Get("authorization")
		if authorizationHeader == "" {
			error2.WriteError(error2.CodeUnauthorized, error2.MessageUnauthorized, w)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		next(w, r, p)
	}
}
