package api

import (
	"github.com/julienschmidt/httprouter"
	"go.uber.org/dig"
	"simple/api/middlware"
	user2 "simple/api/user"
	"simple/app/user"
)

//Router contain main logics
type Router struct {
}

// Route forms route and send to http.server
func (r *Router) Get(dig *dig.Container) *httprouter.Router {
	router := httprouter.New()

	mid := middlware.NewMiddleware()

	userApi := user2.User{}
	if err := dig.Invoke(func(op user.User) {
		userApi = user2.NewApiUser(op)
	}); err != nil {
		panic(err)
	}

	router.POST("/user/create", mid.Auth(userApi.Add))
	router.POST("/user/delete", mid.Auth(userApi.Delete))
	router.GET("/user/get", mid.Auth(userApi.Get))

	return router
}
