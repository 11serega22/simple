package main

import (
	"github.com/jinzhu/configor"
	"net/http"
	"simple/api"
	"simple/cmd/di"
	"simple/configs"
)

func main() {
	cfg := configs.Config{}
	if err := configor.Load(&cfg, "configs/config.yaml"); err != nil {
		panic(err)
	}
	container := di.GetDI(cfg)
	route := new(api.Router).Get(container)

	if err := http.ListenAndServe(":8080", route); err != nil {
		panic(err)
	}
}
