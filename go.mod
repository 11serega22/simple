module simple

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20190424111038-f61b66f89f4a
	github.com/go-pg/pg v8.0.6+incompatible // indirect
	github.com/jinzhu/configor v1.1.1
	github.com/jinzhu/gorm v1.9.11
	github.com/julienschmidt/httprouter v1.3.0
	go.uber.org/dig v1.8.0
	mellium.im/sasl v0.2.1 // indirect
)
