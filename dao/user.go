package dao

import "simple/entity"

type User interface {
	Add(user string, age int) error
	Get() ([]entity.User, error)
	Delete(id int) error
}
