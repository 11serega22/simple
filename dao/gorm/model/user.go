package model

import "simple/entity"

type User struct {
	entity.User
}

func (User) TableName() string {
	return "user"
}
