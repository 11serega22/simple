package gorm

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"simple/dao"
	"simple/dao/gorm/model"
	"simple/entity"
)

type User struct {
	db *gorm.DB
}

func NewDaoUser(db *gorm.DB) dao.User {
	return &User{
		db: db,
	}
}

func (u *User) Add(name string, age int) error {
	user := model.User{User: entity.User{
		Name: name,
		Age:  age,
	}}
	return u.db.Create(&user).Error
}

func (u *User) Get() ([]entity.User, error) {
	rows, err := u.db.
		Table(fmt.Sprintf(`"%s" u`, new(model.User).TableName())).
		Select(`u.id, u.name, u.age`).
		Rows()
	if err != nil {
		return nil, err
	}
	var users []entity.User
	for rows.Next() {
		var user entity.User
		err := rows.Scan(&user.ID, &user.Name, &user.Age)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}

func (u *User) Delete(id int) error {
	return nil
}
