package user

import (
	"simple/dao"
	"simple/entity"
	"simple/service/gravatar"
)

type User struct {
	userDAO dao.User
	avatar gravatar.Gravatar
}

func NewAppUser(userDAO dao.User, avatar gravatar.Gravatar) User {
	return User{
		userDAO: userDAO,
		avatar: avatar,
	}
}

type ArgAdd struct {
	Name string  `valid:"required"`
	Age  int `valid:"required" json:"age"`
}

func (u *User) Add(arg ArgAdd) error {
	return u.userDAO.Add(arg.Name, arg.Age)
}

type Data struct {
	entity.User
	Avatar string
}

func (u *User) List() ([]Data, error) {
	users, err := u.userDAO.Get()
	if err != nil {
		return nil, err
	}
	ud := make([]Data, len(users))
	for k, v := range users {
		ud[k].User = v
	}
	return ud, nil
}

func (u *User) Delete(id int) error {
	return nil
}