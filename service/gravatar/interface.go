package gravatar

import (
	"context"
)

type Data struct {
	Name string
}

type Resp struct {
	Gravatar string
}

type Gravatar interface {
	Get(ctx context.Context, arg Data) (Resp, error)
}
