package gravatar

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

type simple struct {
	httpClient *http.Client
	paymentUrl string
}

func NewSimple() Gravatar {
	timeout := 2 * time.Second
	defaultTransport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   10 * time.Second,
			KeepAlive: 10 * time.Second,
		}).DialContext,
		MaxIdleConns:        100,
		MaxIdleConnsPerHost: 100,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}
	client := &http.Client{
		Transport: defaultTransport,
		Timeout:   timeout,
	}
	return &simple{
		httpClient: client,
		paymentUrl: "",
	}
}

type PostData struct {
	Data
}

func (p *simple) Get(ctx context.Context, arg Data) (payRes Resp, err error) {
	postData := PostData{arg}
	jData, err := json.Marshal(postData)
	if err != nil {
		return payRes, err
	}
	req, err := http.NewRequest("POST", p.paymentUrl, bytes.NewReader(jData))
	if err != nil {
		return payRes, nil
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err := p.httpClient.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return payRes, nil
	}

	err = json.NewDecoder(resp.Body).Decode(&payRes)
	if err != nil {
		return payRes, nil
	}
	_, err = io.Copy(ioutil.Discard, resp.Body)
	if resp.Status != "200 OK" {
		err = errors.New("received unsuccessful status - " + resp.Status)
		return
	}
	return
}

